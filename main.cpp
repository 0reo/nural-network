#include <iostream>
#include "nuron.h"
#include <cstdlib>
#include <stdlib.h>
#include<fstream>
using namespace std;
int lines;

char (*randomize(char inital[][15]))[15] ///randomizes file
{
    int rcount = lines;
    int rrcount = 0;
    int rans[28056]={0};
    uint ran;
    char (* final)[15];

    bool t = true;
    char move[28056][15];
    srand(time(0));
    while (rcount > 0)
    {
        ran = rand()%lines;
        if (rans[ran] == 1)
        {
            t = false;
        }
        else
        {
            rans[ran] = 1;
        }

        if (t == true)
        {
            rcount--;
            for (int i = 0; i < 15; i++)
            {
                move[rcount][i] = inital[ran][i];
            }
        }
        else if (t == false)
        {
            t = true;
        }


    }
    final = move;
    return final;

}

void inputLayer(Nuron inputs[3], int positions[8][8], int count, int x, char (*ran)[15])
{
    inputs[0].resetNuron(); ///clear nuron
    inputs[0].setNuron( positions[ atoi(&ran[x][0]) ] [ atoi(&ran[x][2]) ] );  ///sets first row of nurons with input data(king) and normalizes
    while (inputs[0].getNuron() > count)
    {
        inputs[0].resetNuron(); ///clear nuron
        inputs[0].setNuron( positions[ atoi(&ran[x][0]) ] [ atoi(&ran[x][2]) ] );  ///sets first row of nurons with input data(king) and normalizes
        //cout << inputs[0].getNuron() << endl;
    }
    //cout << inputs[1].getNuron() << " " << ran[x][0] << " " << ran[x][2]<< endl;

    inputs[1].resetNuron(); //clear nuron
    inputs[1].setNuron(positions[atoi(&ran[x][4])][atoi(&ran[x][6])]);  ///sets first row of nurons with input data(rook) and normalizes

    while (inputs[1].getNuron() > count)
    {
        inputs[1].resetNuron(); ///clear nuron
        inputs[1].setNuron( positions[ atoi(&ran[x][0]) ] [ atoi(&ran[x][2]) ] );  ///sets first row of nurons with input data(king) and normalizes
    }

    inputs[2].resetNuron(); ///clear nuron
    inputs[2].setNuron(positions[atoi(&ran[x][8])][atoi(&ran[x][10])]);  ///sets first row of nurons with input data(other king) and normalizes

    while (inputs[2].getNuron() > count)
    {
        inputs[2].resetNuron(); ///clear nuron
        inputs[2].setNuron( positions[ atoi(&ran[x][0]) ] [ atoi(&ran[x][2]) ] );  ///sets first row of nurons with input data(king) and normalizes
    }

}

/******************
second layer starts
******************/
void hiddenLayer(Nuron in[3], Nuron hid[18])
{
    for (int j = 0; j < 18; j++) ///loop for selecting current hidden nuron
    {
        hid[j].resetNuron(); ///clear nuron

        for (int k = 0; k < 3; k++) ///loop for selecting input nuron
        {
            hid[j].setNuron(in[k].getNuron()*hid[j].getWeight()); /// takes outputs from input nurons and sets them into the hidden nurons
        }
    }
}

/******************
second hidden layer
******************/
void hiddenLayer2(Nuron in[18], Nuron hid[9])
{
    for (int j = 0; j < 9; j++) ///loop for selecting current hidden nuron
    {
        hid[j].resetNuron(); ///clear nuron

        for (int k = 0; k < 18; k++) ///loop for selecting input nuron
        {
            hid[j].setNuron(in[k].getNuron()*hid[j].getWeight()); /// takes outputs from input nurons and sets them into the hidden nurons
        }
    }
}
/******************
third layer starts
******************/
int outputLayer(Nuron hid[18], Nuron out[2], int real)
{
    out[0].resetNuron(); ///clear nuron
    out[0].setNuron(999999);
    out[1].resetNuron(); ///clear nuron
    for (int k = 0; k < 18; k++)
    {

        if (hid[k].getNuron() <= out[0].getNuron()) /// find highest output
        {
            out[0].resetNuron(); ///clear nuron
            out[0].setNuron(hid[k].getNuron()); ///sets highest output
            out[1].resetNuron(); ///clear nuron
            out[1].setNuron(k+1);///nuron the output was stored in, aka answer
        }
    }
    return out[1].getNuron();
}


/***************************************
fourth layer starts(if 2 hidden layers)
***************************************/
int outputLayer2(Nuron hid[9], Nuron out[2], int real)
{
    out[0].resetNuron(); ///clear nuron
    out[0].setNuron(999999);
    out[1].resetNuron(); ///clear nuron
    for (int k = 0; k < 9; k++)
    {

        if (hid[k].getNuron() <= out[0].getNuron()) /// find highest output
        {
            out[0].resetNuron(); ///clear nuron
            out[0].setNuron(hid[k].getNuron()); //sets highest output
            out[1].resetNuron(); ///clear nuron
            out[1].setNuron(k+1);///nuron the output was stored in, aka answer
        }
    }
    return out[1].getNuron();
}


void updateWeight(Nuron hid[18], int real, int ans) ///update weight for hirst hiden layer
{
    for (int j = 0; j < 18; j++)
    {
        hid[j].setWeight( real+1-ans, ans);
    }
}

void updateWeight2(Nuron hid[9], int real, int ans)///update weight for hirst hiden layer
{
    for (int j = 0; j < 9; j++)
    {
        hid[j].setWeight( real+1-ans, ans);
    }
}

int main()
{
    ifstream krk;
    krk.open("krk.data");
    char pieces[4][2];
    string result;
    int real[28056];///stores actual answers
    int ans;


    char output[28056][15];
    char (*ran)[15];
    char training;
    char validation;

    if (krk.is_open()) ///read in data
    {
        while (!krk.eof())
        {
            krk >> output[lines];
            lines++;
        }
    }
    krk.close();

    ran = randomize(output);///randomize data

    for (int i = 0; i < lines; i++)///assigne numeric values to non-numeric values of data
    {
        for (int j = 0; j < 10; j++)
        {
            if (ran[i][j] == 'a')
                ran[i][j] = '0';
            if (ran[i][j] == 'b')
                ran[i][j] = '1';
            if (ran[i][j] == 'c')
                ran[i][j] = '2';
            if (ran[i][j] == 'd')
                ran[i][j] = '3';
            if (ran[i][j] == 'e')
                ran[i][j] = '4';
            if (ran[i][j] == 'f')
                ran[i][j] = '5';
            if (ran[i][j] == 'g')
                ran[i][j] = '6';
            if (ran[i][j] == 'h')
                ran[i][j] = '7';
        }
        for (int j = 12; j == 12; j++)
        {
            if (ran[i][j] == '1')
            {
                if (ran[i][j+1] == '0' || ran[i][j+1] == '1' || ran[i][j+1] == '2' || ran[i][j+1] == '3' || ran[i][j+1] == '4' || ran[i][j+1] == '5' ||
                        ran[i][j+1] == '6' || ran[i][j+1] == '7' || ran[i][j+1] == '8' || ran[i][j+1] == '9')
                {
                    real[i] = 10+ atoi(&ran[i][j+1]);
                }
                else
                {
                    real[i] = 1;
                }
            }
            else
            {
                real[i] = atoi(&ran[i][j]);
            }
        }
    }


    Nuron inputs[3], hidden[18], hidden2[9], final[2]; ///nurons
    int positions[8][8];///positions on board
    int count, cc;///counters
    count = 0.0;


    for (int i = 0; i < 8; i++)///a-g
    {
        for (int j = 0; j < 8; j++)///1-8
        {
            positions[i][j] =  count; ///represents position i,j (example, a,1 or b,5, etc)
            count++;
        }
    }


    for (int x = 0; x < lines*0.7-1; x++)/**training**/
    {

        inputLayer(inputs, positions, count, x, ran);///sets values for input layers
        for (int i = 0; i < 18; i++)
        {
            hidden[i].resetNuron();
        }
        hiddenLayer(inputs, hidden);///sets hidden layer 1
        hiddenLayer2(hidden, hidden2);///sets hidden layer 2
        ans = -1;
        ans = outputLayer2(hidden2, final, real[x]);///gets answer
        cc = 0;//counter

        while (ans-1 > real[x]+1 || ans-1 < real[x]-1 )
        {
            cc++;

            updateWeight(hidden, real[x], ans);///update weight for hidden layer 1
            hiddenLayer(inputs, hidden);///get new values for hidden layer one

            updateWeight2(hidden2, real[x], ans);///update weight for hidden layer 2
            hiddenLayer2(hidden, hidden2);///get new value for hidden layer 2

            ans = outputLayer(hidden2, final, real[x]);///set new answer
        }
        cout << final[1].getNuron() << "..." <<real[x]<<endl;///print answer recived, and real answer
    }

    cout << "done" << endl;
    cin.get();///pause

    for (int x = lines*0.7-1; x < lines; x++)/**testing**/
    {
        inputLayer(inputs, positions, count, x, ran);///sets values for input layer
        hiddenLayer(inputs, hidden);///sets values for first hidden layer
        hiddenLayer2(hidden, hidden2);///sets hidden layer 2
        ans = -1;
        ans = outputLayer(hidden, final, real[x]);///sets new answer
        int cc = 0;
        cout << final[1].getNuron() << "..." <<cc<<endl;
        cin.get();///pause
    }

    return 0;
}
