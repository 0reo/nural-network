#include "nuron.h"
#include <cstdlib>
#include <iostream>

using namespace std;

Nuron::Nuron()
{
    a = 0.05;
    n = 0.01;
    int r = rand()%25;
    //weight = ((double(r)/RAND_MAX)*0.15)+0.000001;
    weight = double(r)/1000.0;
    weightOld = 0.0;
    value = 0.0;
    trigger = false;
    //ctor
}

void Nuron::setNuron(double i)///set value(algamation of all incoming values)
{
    value += i;
}

double Nuron::getNuron()///return vaue
{
    return value;
}

void Nuron::resetNuron()///clear value
{
    value = 0.0;
}

void Nuron::setWeight(double d, double o)///update weight
{
    weightNew = weight+ a*(weight-weightOld)+n*d*value;
    weightOld = weight;
    weight = weightNew;
}

double Nuron::getWeight()///return weight
{
    return weight;
}

void Nuron::setLearningRate(double i)
{
    a = i;
}

void Nuron::setMomentum(double i)
{
    n = i;
}

Nuron::~Nuron()
{
    //dtor
}
