#ifndef NURON_H
#define NURON_H


class Nuron
{
public:
    Nuron();
    virtual ~Nuron();
    void setNuron(double i);
    double getNuron();
    double getWeight();
    void resetNuron();
    void setWeight(double d, double o);
    void setLearningRate(double i);
    double getLearningRate();
    void setMomentum(double i);
    double getMomentum();
    double getValue();
    void setValue();

protected:
private:
    double weight, weightOld, weightNew;///[] represents node
    double value;///value
    double a;///learning paramator, aka speed
    double n;///momemtum parametor
    double d;///activation function
    bool trigger;
};

#endif // NURON_H
